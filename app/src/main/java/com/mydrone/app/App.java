package com.mydrone.app;

import android.app.Application;
import android.content.Context;

import com.mydrone.apis.ApiService;
import com.google.firebase.FirebaseApp;

import androidx.multidex.MultiDex;



public class App extends Application {

    private static ApiService apiService;

    @Override
    public void onCreate() {
        super.onCreate();
        FirebaseApp.initializeApp(getApplicationContext());
        RestClient restClient = new RestClient();
        apiService = restClient.getApiService();

    }

    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    public static ApiService getService() {
        return apiService;
    }
}
