package com.mydrone.modules.splashscreen;


import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.mydrone.R;
import com.mydrone.base.BaseMvpLceFragment;
import com.mydrone.listeners.FragmentInteractionListener;
import com.mydrone.models.ABase.RequestBase;
import com.mydrone.modules.home.HomeFragment;
import com.mydrone.modules.login.LoginFragment;
import com.mydrone.utility.CommonUtilities;
import com.mydrone.utility.FragmentStack;
import com.mydrone.utility.Preferences;

import butterknife.BindView;

import static com.mydrone.BuildConfig.VERSION_NAME;

public class SplashScreenFragment extends BaseMvpLceFragment<RelativeLayout, RequestBase, XSplashScreenView, SplashScreenPresenter>
        implements XSplashScreenView, FragmentInteractionListener {
    FragmentStack fragmentStack;
    @BindView(R.id.errorView)
    TextView errorView;
    @BindView(R.id.loadingView)
    ProgressBar loadingView;
    @BindView(R.id.tvVersion)
    TextView tvVersion;
    @BindView(R.id.container2)
    FrameLayout container2;
    @BindView(R.id.contentView)
    RelativeLayout contentView;
    private String author_id = "";

    public SplashScreenFragment() {

    }

    @Override
    public SplashScreenPresenter createPresenter() {
        return new SplashScreenPresenter(getContext());
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_splash_screen;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Preferences preferences = new Preferences(getContext());
        author_id = preferences.getPreferencesString("author_id");
        CommonUtilities.finishApp(view, getActivity());
        tvVersion.setText("Version "+VERSION_NAME);
        fragmentStack = new FragmentStack(getActivity(), getActivity().getSupportFragmentManager(), R.id.container2);
        if (author_id != null) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    fragmentStack.push(new HomeFragment(), author_id);
                }
            }, 1000);
        } else {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    fragmentStack.push(new LoginFragment(), null);
                }
            }, 1000);
        }

    }

    @Override
    protected String getErrorMessage(Throwable e, boolean pullToRefresh) {
        return e.getMessage();
    }

    @Override
    public void setData(RequestBase data) {

    }

    @Override
    public void loadData(boolean pullToRefresh) {

    }

    @Override
    public void gotoPage(Fragment page, boolean shouldReplace, String tagName) {
        if (shouldReplace) {
            fragmentStack.push(page, tagName);
        } else {
            fragmentStack.pushAdd(page, tagName);
        }
    }

    @Override
    public void back() {

    }

}