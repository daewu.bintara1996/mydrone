package com.mydrone.modules.splashscreen;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

public interface XSplashScreenPresenter extends MvpPresenter<XSplashScreenView> {

}