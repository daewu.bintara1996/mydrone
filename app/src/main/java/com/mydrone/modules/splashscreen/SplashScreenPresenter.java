package com.mydrone.modules.splashscreen;

import android.content.Context;

import com.mydrone.base.BaseRxLcePresenter;
import com.mydrone.models.ABase.RequestBase;

public class SplashScreenPresenter extends BaseRxLcePresenter<XSplashScreenView, RequestBase>
        implements XSplashScreenPresenter {

    public SplashScreenPresenter(Context context) {
        super(context);
    }

}