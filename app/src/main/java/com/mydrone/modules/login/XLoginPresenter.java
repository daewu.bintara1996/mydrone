package com.mydrone.modules.login;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

public interface XLoginPresenter extends MvpPresenter<XLoginView> {

    void checkLogin(String toString, boolean pullToRefresh);
}