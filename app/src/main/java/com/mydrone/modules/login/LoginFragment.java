package com.mydrone.modules.login;

import android.app.AlertDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.mydrone.R;
import com.mydrone.base.BaseMvpLceFragment;
import com.mydrone.listeners.FragmentInteractionListener;
import com.mydrone.models.Login;
import com.mydrone.modules.verifyotp.VerifyOtpFragment;
import com.mydrone.utility.CommonUtilities;
import com.mydrone.utility.FragmentStack;

import butterknife.BindView;
import butterknife.ButterKnife;
import dmax.dialog.SpotsDialog;

public class LoginFragment extends BaseMvpLceFragment<RelativeLayout, Login, XLoginView, LoginPresenter>
        implements XLoginView, FragmentInteractionListener {

    @BindView(R.id.contentView)
    RelativeLayout contentView;
    @BindView(R.id.errorView)
    TextView errorView;
    @BindView(R.id.loadingView)
    ProgressBar loadingView;
    @BindView(R.id.etNoHp)
    EditText etNoHp;
    @BindView(R.id.btnNext)
    Button btnNext;

    private AlertDialog customDialog ;
    private FragmentStack fragmentStack;

    public LoginFragment() {

    }

    @Override
    public LoginPresenter createPresenter() {
        return new LoginPresenter(getContext());
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_login;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        showContent();
        customDialog = new SpotsDialog(getContext(), R.style.CustomDialog);
        fragmentStack = new FragmentStack(getActivity(), getActivity().getSupportFragmentManager(), R.id.container2);

        CommonUtilities.finishApp(view, getActivity());
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!etNoHp.getText().toString().isEmpty()){
                    CommonUtilities.hideSoftKeyboard(getActivity());
                    loadData(true);
                }
            }
        });
    }

    @Override
    protected String getErrorMessage(Throwable e, boolean pullToRefresh) {
        customDialog.dismiss();
        return "Gagal login, pastikan No. Hp yang dimasukkan benar.";
    }

    @Override
    public void setData(Login data) {
        customDialog.dismiss();
        showToast("Kode OTP berhasil dikirim.");
        fragmentStack.push(new VerifyOtpFragment(), data.getAuthorId()+"");
    }

    @Override
    public void loadData(boolean pullToRefresh) {
        customDialog.show();
        getPresenter().checkLogin(etNoHp.getText().toString(), pullToRefresh);

    }

    @Override
    public void gotoPage(Fragment page, boolean shouldReplace, String tagName) {
        if (shouldReplace) {
            fragmentStack.push(page, tagName);
        } else {
            fragmentStack.pushAdd(page, tagName);
        }
    }

    @Override
    public void back() {

    }

}