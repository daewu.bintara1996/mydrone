package com.mydrone.modules.login;

import android.content.Context;

import com.mydrone.app.App;
import com.mydrone.base.BaseRxLcePresenter;
import com.mydrone.models.Login;

import java.util.HashMap;

public class LoginPresenter extends BaseRxLcePresenter<XLoginView, Login>
        implements XLoginPresenter {

    public LoginPresenter(Context context) {
        super(context);
    }

    @Override
    public void checkLogin(String toString, boolean pullToRefresh) {
        HashMap<String, String> paras = new HashMap<>();
        paras.put("act", "login");
        paras.put("phone", toString);
        subscribe(App.getService().apiLoginRequest(paras), pullToRefresh);
    }

}