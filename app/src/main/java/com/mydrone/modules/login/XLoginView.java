package com.mydrone.modules.login;

import com.hannesdorfmann.mosby3.mvp.lce.MvpLceView;
import com.mydrone.models.ABase.RequestBase;
import com.mydrone.models.Login;

public interface XLoginView extends MvpLceView<Login> {

}