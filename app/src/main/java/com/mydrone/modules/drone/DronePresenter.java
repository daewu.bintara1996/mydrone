package com.mydrone.modules.drone;

import android.content.Context;

import com.mydrone.app.App;
import com.mydrone.base.BaseRxLcePresenter;
import com.mydrone.models.Drone.DroneUpdate;

import java.util.HashMap;

public class DronePresenter extends BaseRxLcePresenter<XDroneView, DroneUpdate>
        implements XDronePresenter {

    public DronePresenter(Context context) {
        super(context);
    }

    @Override
    public void updateStatusOnline(String online_status, String author_id, String drone_id) {
        HashMap<String,String> params = new HashMap<>();
        params.put("act", "update_online_status");
        params.put("drone_author_id", author_id);
        params.put("online_status", online_status);
        params.put("drone_id", drone_id);
        subscribe(App.getService().apiDroneUpdateTest(params));
    }
}