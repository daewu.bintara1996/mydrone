package com.mydrone.modules.drone.listener;

import android.location.Location;

public interface GPSCallback {
    public abstract void onGPSUpdate(Location location);
}

