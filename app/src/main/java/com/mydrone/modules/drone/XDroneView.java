package com.mydrone.modules.drone;

import com.hannesdorfmann.mosby3.mvp.lce.MvpLceView;
import com.mydrone.models.Drone.DroneUpdate;

public interface XDroneView extends MvpLceView<DroneUpdate> {

}