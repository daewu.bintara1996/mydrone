package com.mydrone.modules.drone;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.BatteryManager;
import android.os.Bundle;
import android.os.PowerManager;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.hannesdorfmann.fragmentargs.annotation.Arg;
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.mydrone.R;
import com.mydrone.base.BaseMvpLceFragment;
import com.mydrone.listeners.FragmentInteractionListener;
import com.mydrone.models.Drone.DroneUpdate;
import com.mydrone.models.Services.TrackerService;
import com.mydrone.models.Speed;
import com.mydrone.modules.drone.listener.GPSCallback;
import com.mydrone.modules.drone.manager.GPSManager;
import com.mydrone.utility.Preferences;
import com.squareup.picasso.Picasso;
import com.wang.avi.AVLoadingIndicatorView;

import java.math.BigDecimal;
import java.net.HttpURLConnection;

import butterknife.BindView;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;
import dmax.dialog.SpotsDialog;

import static android.content.Context.LOCATION_SERVICE;
import static com.mydrone.utility.CommonUtilities.round;

@FragmentWithArgs
public class DroneFragment extends BaseMvpLceFragment<RelativeLayout, DroneUpdate, XDroneView, DronePresenter>
        implements XDroneView, FragmentInteractionListener, GPSCallback {

    @Arg
    String title;
    @Arg(required = false)
    String sTitle;
    @Arg(required = false)
    String drone_id;
    @Arg(required = false)
    String drone_status;

    @BindView(R.id.imgDrone)
    CircularImageView imgDrone;
    @BindView(R.id.tvDroneName)
    TextView tvDroneName;
    @BindView(R.id.tvStatusMachine)
    TextView tvStatusMachine;
    @BindView(R.id.btnStart)
    Button btnStart;
    @BindView(R.id.contentView)
    RelativeLayout contentView;
    @BindView(R.id.errorView)
    TextView errorView;
    @BindView(R.id.loadingView)
    ProgressBar loadingView;
    @BindView(R.id.btnStop)
    Button btnStop;
    @BindView(R.id.btnBack)
    Button btnBack;
    @BindView(R.id.relativeLayout)
    RelativeLayout relativeLayout;
    @BindView(R.id.tvOnlineStatus)
    TextView tvOnlineStatus;
    @BindView(R.id.loadingDrone)
    AVLoadingIndicatorView loadingDrone;
    @BindView(R.id.tvDone)
    TextView tvDone;

    private GPSManager gpsManager = null;
    private double speed = 0.0;
    Boolean isGPSEnabled = false;
    LocationManager locationManager;
    double currentSpeed, kmphSpeed;

    AlertDialog alertDialog;
    private String online_status, longitude, latitude;
    private static HttpURLConnection con;
    private static final int PERMISSIONS_REQUEST = 1;
    private DatabaseReference mFirebaseDatabase;
    private FirebaseDatabase mFirebaseInstance;
    protected PowerManager.WakeLock mWakeLock;
    BroadcastReceiver getmBatInfoReceiver;

    public DroneFragment() {

    }

    @Override
    public DronePresenter createPresenter() {
        return new DronePresenter(getContext());
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_drone;
    }

    @SuppressLint("InvalidWakeLockTag")
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        showContent();

        final PowerManager pm = (PowerManager) getActivity().getSystemService(Context.POWER_SERVICE);
        this.mWakeLock = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, "My Tag");
        this.mWakeLock.acquire();
        getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        alertDialog = new SpotsDialog(getActivity());
        tvDroneName.setText(title);
        tvOnlineStatus.setText("Offline");
        tvOnlineStatus.setBackground(getResources().getDrawable(R.drawable.bg_corner_red));
        Picasso.get()
                .load(sTitle)
                .into(imgDrone);

        if (drone_status.equals("tested")) {
            SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(getContext());
            sweetAlertDialog.setTitleText("PERINGATAN")
                    .setContentText("Drone telah diuji, apakah ingin menguji lagi?")
                    .showCancelButton(true)
                    .setCancelText("Batal")
                    .setConfirmClickListener(sweetAlertDialog1 -> sweetAlertDialog.dismiss())
                    .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            sweetAlertDialog.dismiss();
                            back();
                        }
                    })
                    .show();
        }

        permissionLocation();
        OnViewCLicked(view);

        mFirebaseInstance = FirebaseDatabase.getInstance();

        // get reference to 'mydrone' node
        mFirebaseDatabase = mFirebaseInstance.getReference("mydrone");

        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP) {
                    if (tvStatusMachine.getText().equals(getString(R.string.text_memuat)) && tvOnlineStatus.getText().toString().equals("Offline")) {
                        back();
                    } else {
                        showToast("Matikan proses terlebih dahulu.");
                    }
                    return true;
                }
                return false;
            }
        });

    }

    @OnClick({R.id.btnStart, R.id.btnStop, R.id.btnBack})
    public void OnViewCLicked(View view){
        switch (view.getId()){
            case R.id.btnStart :
                alertDialog.show();
                loadingDrone.setVisibility(View.VISIBLE);
                online_status = "online";
                getmBatInfoReceiver = new BroadcastReceiver() {
                    @Override
                    public void onReceive(Context ctxt, Intent intent) {
                        int level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, 0);
//                        showToast(String.valueOf(level) + "%");
                        mFirebaseDatabase.child("drone_id_"+drone_id).child("batteryStart").setValue(String.valueOf(level) + "%");
                    }
                };
                getContext().registerReceiver(getmBatInfoReceiver, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
                loadData(false);
                break;
            case R.id.btnStop :
                getmBatInfoReceiver = new BroadcastReceiver() {
                    @Override
                    public void onReceive(Context ctxt, Intent intent) {
                        int level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, 0);
//                        showToast(String.valueOf(level) + "%");
                        mFirebaseDatabase.child("drone_id_"+drone_id).child("batteryStop").setValue(String.valueOf(level) + "%");
                    }
                };
                getContext().registerReceiver(getmBatInfoReceiver, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
                online_status = "offline";
                loadData(false);
                gpsManager.stopListening();
                gpsManager.setGPSCallback(null);
                gpsManager = null;
                break;
            case R.id.btnBack :
                if (tvStatusMachine.getText().equals(getString(R.string.text_memuat)) && tvOnlineStatus.getText().toString().equals("Offline")) {
                    back();
                } else {
                    showToast("Matikan proses terlebih dahulu.");
                }
                break;
        }
    }


    private void onStartProcess() {
        locationManager = (LocationManager) getContext().getSystemService(LOCATION_SERVICE);
        gpsManager = new GPSManager(getContext());
        isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        if (isGPSEnabled) {
            gpsManager.startListening(getContext().getApplicationContext());
            gpsManager.setGPSCallback(this);
            btnStop.setVisibility(View.VISIBLE);
            btnStart.setVisibility(View.GONE);
        } else {
            gpsManager.showSettingsAlert();
        }
    }

    private void permissionLocation() {
        try {
            if (ContextCompat.checkSelfPermission(getContext().getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 101);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected String getErrorMessage(Throwable e, boolean pullToRefresh) {
        return e.getMessage();
    }

    @Override
    public void setData(DroneUpdate data) {
//        showToast(data.getAuthorRegId());
        if (online_status.equals("offline")) {
            tvDone.setText("Stopped.");
            tvStatusMachine.setText(getString(R.string.text_memuat));
            btnStop.setVisibility(View.VISIBLE);
            btnStart.setVisibility(View.VISIBLE);
            alertDialog.dismiss();
            tvOnlineStatus.setBackground(getResources().getDrawable(R.drawable.bg_corner_red));
            tvOnlineStatus.setText("Offline");
        } else {
            tvDone.setText("Ready.");
            tvOnlineStatus.setBackground(getResources().getDrawable(R.drawable.bg_corner_green));
            tvOnlineStatus.setText("Online");
            onStartProcess();
        }
    }

    @Override
    public void loadData(boolean pullToRefresh) {
        alertDialog.show();
        Preferences preferences = new Preferences(getContext());
        String author_id = preferences.getPreferencesString("author_id");
        getPresenter().updateStatusOnline(online_status, author_id, drone_id);
    }

    @Override
    public void gotoPage(Fragment page, boolean shouldReplace, String tagName) {

    }

    @Override
    public void back() {
        FragmentManager fm = getActivity().getSupportFragmentManager();
        fm.popBackStack();
    }

    @Override
    public void onGPSUpdate(Location location) {
        alertDialog.dismiss();
        loadingDrone.setVisibility(View.GONE);

        speed = location.getSpeed();
        currentSpeed = round(speed, 3, BigDecimal.ROUND_HALF_UP);
        kmphSpeed = round((currentSpeed * 3.6), 3, BigDecimal.ROUND_HALF_UP);
        tvStatusMachine.setText(kmphSpeed + " Km/h");
        longitude = location.getLongitude()+"";
        latitude = location.getLatitude()+"";

        mFirebaseDatabase.child("drone_id_"+drone_id).child("speed").setValue(kmphSpeed+" Km/h");
        mFirebaseDatabase.child("drone_id_"+drone_id).child("longitude").setValue(longitude);
        mFirebaseDatabase.child("drone_id_"+drone_id).child("latitude").setValue(latitude);
        getmBatInfoReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context ctxt, Intent intent) {
                int level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, 0);
//                        showToast(String.valueOf(level) + "%");
                mFirebaseDatabase.child("drone_id_"+drone_id).child("batteryStop").setValue(String.valueOf(level) + "%");
            }
        };
        getContext().registerReceiver(getmBatInfoReceiver, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));

    }

    @Override
    public void onDestroy() {
        this.mWakeLock.release();
        super.onDestroy();
    }
}