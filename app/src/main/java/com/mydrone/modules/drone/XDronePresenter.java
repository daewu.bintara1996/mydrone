package com.mydrone.modules.drone;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

public interface XDronePresenter extends MvpPresenter<XDroneView> {

    void updateStatusOnline(String online_status, String author_id, String drone_id);
}