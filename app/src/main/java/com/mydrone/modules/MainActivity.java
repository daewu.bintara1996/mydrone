package com.mydrone.modules;

import android.os.Bundle;
import android.widget.Toast;

import com.mydrone.R;
import com.mydrone.listeners.FragmentInteractionListener;
import com.mydrone.modules.splashscreen.SplashScreenFragment;
import com.mydrone.utility.FragmentStack;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity
        implements FragmentInteractionListener {

    private FragmentStack fragmentStack;
    private static final int TIME_DELAY = 2000;
    private static long back_pressed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        fragmentStack = new FragmentStack(this, getSupportFragmentManager(), R.id.container);
        fragmentStack.push(new SplashScreenFragment(), null);

        if (getIntent().getBooleanExtra("KILL_APP", false)) { finish(); }
        if (getIntent().getBooleanExtra("KILL_APP", false)) { finish(); }

    }

    @Override public void gotoPage(Fragment page, boolean shouldReplace, String tagName) {

        if (shouldReplace) {
            fragmentStack.push(page, tagName);
        } else {
            fragmentStack.pushAdd(page, tagName);
        }

    }

    @Override public void back() {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        if (fragmentStack.size() > 1) {
            fragmentStack.back();
        } else {
            if (!fragmentStack.back()) {
                if (back_pressed + TIME_DELAY > System.currentTimeMillis()) {
                    finish();
                } else {
                    Toast.makeText(getBaseContext(), "Tekan lagi untuk keluar",
                            Toast.LENGTH_SHORT).show();
                }
                back_pressed = System.currentTimeMillis();
            }
        }
    }
}
