package com.mydrone.modules.verifyotp;

import com.hannesdorfmann.mosby3.mvp.lce.MvpLceView;
import com.mydrone.models.VerivyOtp;

public interface XVerifyOtpView extends MvpLceView<VerivyOtp> {

}