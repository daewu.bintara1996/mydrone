package com.mydrone.modules.verifyotp;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

public interface XVerifyOtpPresenter extends MvpPresenter<XVerifyOtpView> {

    void onVerifyOtp(String author_id, String otp, boolean pullToRefresh);
}