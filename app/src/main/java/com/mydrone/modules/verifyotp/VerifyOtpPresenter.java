package com.mydrone.modules.verifyotp;

import android.content.Context;

import com.mydrone.app.App;
import com.mydrone.base.BaseRxLcePresenter;
import com.mydrone.models.VerivyOtp;

import java.util.HashMap;

public class VerifyOtpPresenter extends BaseRxLcePresenter<XVerifyOtpView, VerivyOtp>
        implements XVerifyOtpPresenter {

    public VerifyOtpPresenter(Context context) {
        super(context);
    }

    @Override
    public void onVerifyOtp(String author_id, String otp, boolean pullToRefresh) {
        HashMap<String, String> param = new HashMap<>();
        param.put("act", "verify_otp");
        param.put("author_id", author_id);
        param.put("otp_code", otp);

        subscribe(App.getService().apiVerifyOtpRequest(param), pullToRefresh);
    }
}