package com.mydrone.modules.verifyotp;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.mydrone.R;
import com.mydrone.base.BaseMvpLceFragment;
import com.mydrone.listeners.FragmentInteractionListener;
import com.mydrone.models.VerivyOtp;
import com.mydrone.modules.MainActivity;
import com.mydrone.modules.home.HomeFragment;
import com.mydrone.utility.CommonUtilities;
import com.mydrone.utility.FragmentStack;
import com.mydrone.utility.Preferences;

import butterknife.BindView;
import butterknife.ButterKnife;
import dmax.dialog.SpotsDialog;

public class VerifyOtpFragment extends BaseMvpLceFragment<RelativeLayout, VerivyOtp, XVerifyOtpView, VerifyOtpPresenter>
        implements XVerifyOtpView, FragmentInteractionListener {

    @BindView(R.id.errorView)
    TextView errorView;
    @BindView(R.id.loadingView)
    ProgressBar loadingView;
    @BindView(R.id.etKodeOtp)
    EditText etKodeOtp;
    @BindView(R.id.btnResendOtp)
    Button btnResendOtp;
    @BindView(R.id.lnView)
    LinearLayout lnView;
    @BindView(R.id.contentView)
    RelativeLayout contentView;
    @BindView(R.id.btnLogin)
    Button btnLogin;

    Intent intent;
    private AlertDialog customDialog ;
    private FragmentStack fragmentStack;
    private String Otp = "", author_id = "";
    Handler handler = new Handler();

    public VerifyOtpFragment() {

    }

    @Override
    public VerifyOtpPresenter createPresenter() {
        return new VerifyOtpPresenter(getContext());
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_verify_otp;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        showContent();
        customDialog = new SpotsDialog(getContext(), R.style.CustomDialog);
        author_id = getArguments().getString("the_key");
        lnView.setVisibility(View.GONE);
        onCreateTimeRemaining();
        onBtnLogin();
    }

    @Override
    protected String getErrorMessage(Throwable e, boolean pullToRefresh) {
        customDialog.dismiss();
        return e.getMessage();
    }

    @Override
    public void setData(VerivyOtp data) {
        handler.removeCallbacksAndMessages(null);
        customDialog.dismiss();
        Preferences preferences = new Preferences(getContext());
        preferences.savePreferences("author_id", author_id);
        preferences.savePreferences("otp_code", data.getOtpCode());
        intent = new Intent(getActivity(), MainActivity.class);
        startActivity(intent);
        getActivity().finish();

    }

    @Override
    public void loadData(boolean pullToRefresh) {
        getPresenter().onVerifyOtp(author_id, etKodeOtp.getText().toString(), pullToRefresh);
    }

    @Override
    public void gotoPage(Fragment page, boolean shouldReplace, String tagName) {

    }

    @Override
    public void back() {
        FragmentManager fm = getActivity().getSupportFragmentManager();
        fm.popBackStack();
        CommonUtilities.hideSoftKeyboard(getActivity());
    }

    private void onCreateTimeRemaining() {
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                lnView.setVisibility(View.VISIBLE);
            }
        }, 80000);
        btnResendOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                back();
            }
        });
    }

    private void onBtnLogin() {
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!etKodeOtp.getText().toString().isEmpty()){
                    CommonUtilities.hideSoftKeyboard(getActivity());
                    customDialog.show();
                    loadData(true);
                }
            }
        });
    }
}