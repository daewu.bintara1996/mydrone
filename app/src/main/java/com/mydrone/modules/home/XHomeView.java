package com.mydrone.modules.home;

import com.hannesdorfmann.mosby3.mvp.lce.MvpLceView;
import com.mydrone.models.Drone.RequestDrone;

public interface XHomeView extends MvpLceView<RequestDrone> {

}