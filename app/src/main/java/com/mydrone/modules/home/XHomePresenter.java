package com.mydrone.modules.home;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

public interface XHomePresenter extends MvpPresenter<XHomeView> {

    void onLoadDrone(String author_id, boolean pullToRefresh, int page);
}