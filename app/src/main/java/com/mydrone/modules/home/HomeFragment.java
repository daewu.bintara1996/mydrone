package com.mydrone.modules.home;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.PopupMenu;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.mikhaellopez.circularimageview.CircularImageView;
import com.mydrone.R;
import com.mydrone.adapters.DroneAdapter;
import com.mydrone.base.BaseLceRefreshFragment;
import com.mydrone.listeners.FragmentInteractionListener;
import com.mydrone.models.Drone.RequestDrone;
import com.mydrone.modules.MainActivity;
import com.mydrone.modules.drone.DroneFragmentBuilder;
import com.mydrone.utility.CommonUtilities;
import com.mydrone.utility.FunctionMedia;
import com.mydrone.utility.Preferences;
import com.mydrone.utility.recyclerview.EndlessRecyclerOnScrollListener;
import com.mydrone.utility.recyclerview.ItemClickSupport;
import com.squareup.picasso.Picasso;
import com.tbruyelle.rxpermissions2.RxPermissions;
import com.theartofdev.edmodo.cropper.CropImage;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;
import dmax.dialog.SpotsDialog;

public class HomeFragment extends BaseLceRefreshFragment<SwipeRefreshLayout, RequestDrone, XHomeView, HomePresenter>
        implements XHomeView , FragmentInteractionListener {


    @BindView(R.id.rvDrone)
    RecyclerView rvDrone;
    @BindView(R.id.contentView)
    SwipeRefreshLayout contentView;
    @BindView(R.id.errorView)
    TextView errorView;
    @BindView(R.id.loadingView)
    ProgressBar loadingView;
    @BindView(R.id.tvAuthorName)
    TextView tvAuthorName;
    @BindView(R.id.btnLogout)
    Button btnLogout;
    @BindView(R.id.imgAuthor)
    CircularImageView imgAuthor;

    DroneAdapter droneAdapter;
    private String author_id;
    private AlertDialog alertDialog;
    EndlessRecyclerOnScrollListener scrollListener;
    Uri uriImage;


    public HomeFragment() {

    }

    @Override
    public HomePresenter createPresenter() {
        return new HomePresenter(getContext());
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_home;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        showContent();
        ButterKnife.bind(this, view);
        Preferences preferences = new Preferences(getContext());
        author_id = preferences.getPreferencesString("author_id");
        alertDialog = new SpotsDialog(getActivity());
        alertDialog.show();

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL,false);

        droneAdapter = new DroneAdapter();
        scrollListener = new EndlessRecyclerOnScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                loadData(false);
            }
        };
        rvDrone.addOnScrollListener(scrollListener);
        rvDrone.setHasFixedSize(true);
        rvDrone.setLayoutManager(layoutManager);
        rvDrone.setAdapter(droneAdapter);
        loadData(false);

        onLogoutClicked();
        onBackPressed(view);
        permissionLocation();
    }

    private void permissionLocation() {
        try {
            if (ContextCompat.checkSelfPermission(getContext().getApplicationContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED ) {
                ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, 101);
            }
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    private void onBackPressed(View view) {
        CommonUtilities.finishApp(view, getActivity());
    }

    private void onLogoutClicked() {
        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(getContext(), SweetAlertDialog.WARNING_TYPE);
                sweetAlertDialog.setTitleText("LOGOUT")
                        .setContentText("Yakin untuk keluar akun?").showCancelButton(true).setCancelText("Batal")
                        .setCancelClickListener(sweetAlertDialog1 -> sweetAlertDialog.dismissWithAnimation())
                        .setConfirmClickListener(
                         new SweetAlertDialog.OnSweetClickListener() {
                             @Override
                             public void onClick(SweetAlertDialog sweetAlertDialog) {
                                 Preferences preferences = new Preferences(getContext());
                                 preferences.clearAllPreferences();
                                 sweetAlertDialog.dismiss();
                                 loadingView.setVisibility(View.VISIBLE);
                                 Intent intent = new Intent(getActivity(), MainActivity.class);
                                 startActivity(intent);
                                 getActivity().finish();
                             }
                         }
                        ).show();

            }
        });
    }



    @Override
    protected String getErrorMessage(Throwable e, boolean pullToRefresh) {
        alertDialog.dismiss();
        return "Gagal terhubung ke server.";
    }

    @Override
    public void setData(RequestDrone data) {
        alertDialog.dismiss();
        droneAdapter.setItems(data.getDroneList());
        tvAuthorName.setText(data.getAuthorName());
        Picasso.get()
                .load(data.getAuthorImage())
                .into(imgAuthor);
        ItemClickSupport.addTo(rvDrone).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                mListener.gotoPage(new DroneFragmentBuilder(""+droneAdapter.getItem(position).getDroneName())
                        .sTitle(droneAdapter.getItem(position).getDroneImage())
                        .drone_id(droneAdapter.getItem(position).getDroneId()+"")
                        .drone_status(droneAdapter.getItem(position).getDroneStatus())
                        .build(), false, "drone"
                );
            }
        });
    }

    @Override
    public void loadData(boolean pullToRefresh) {
        if (pullToRefresh) {
            droneAdapter.clear();
            contentView.setRefreshing(false);
        }
        int page = droneAdapter.getItemCount();
        getPresenter().onLoadDrone(author_id, pullToRefresh, page);
    }

    @Override
    public void gotoPage(Fragment page, boolean shouldReplace, String tagName) {
    }

    @Override
    public void back() {
        FragmentManager fm = getActivity().getSupportFragmentManager();
        fm.popBackStack();
    }

}