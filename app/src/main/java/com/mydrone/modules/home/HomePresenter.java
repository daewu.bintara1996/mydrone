package com.mydrone.modules.home;

import android.content.Context;

import com.mydrone.app.App;
import com.mydrone.base.BaseRxLcePresenter;
import com.mydrone.models.Drone.RequestDrone;

import java.util.HashMap;

public class HomePresenter extends BaseRxLcePresenter<XHomeView, RequestDrone>
        implements XHomePresenter {

    public HomePresenter(Context context) {
        super(context);
    }

    @Override
    public void onLoadDrone(String author_id, boolean pullToRefresh, int page) {
        HashMap<String, String> params = new HashMap<>();
        params.put("act", "drone_by_author");
        params.put("drone_author_id", author_id);
        params.put("page", page+"");
        subscribe(App.getService().apiDroneRequest(params));

    }
}