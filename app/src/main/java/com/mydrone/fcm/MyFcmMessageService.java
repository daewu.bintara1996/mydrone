package com.mydrone.fcm;

import com.mydrone.utility.Logs;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;

public class MyFcmMessageService extends FirebaseMessagingService {

    @Override public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        Logs.d("MyFcmMessageService => onMessageReceived ==> " + new Gson().toJson(remoteMessage.getData()));
    }
}
