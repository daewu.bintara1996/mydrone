package com.mydrone.adapters;

import com.mydrone.base.BaseRecyclerViewAdapter;
import com.mydrone.holder.ItemDroneHolder;
import com.mydrone.models.Drone.DroneList;


public class DroneAdapter extends BaseRecyclerViewAdapter<DroneList, ItemDroneHolder> {

    public DroneAdapter() {
        super(ItemDroneHolder.class);
    }

    @Override
    protected void populateViewHolder(ItemDroneHolder viewHolder, DroneList item, int position) {
        viewHolder.bindView(item);
    }
}