package com.mydrone.listeners;


import android.os.Bundle;

import androidx.fragment.app.Fragment;

public interface FragmentInteractionListener {
    void gotoPage(Fragment page, boolean shouldReplace, String tagName);
    void back();
}
