package com.mydrone.base;

import android.content.Context;

import com.mydrone.utility.Preferences;
import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;
import com.hannesdorfmann.mosby3.mvp.MvpView;

public class BasePresenter<V extends MvpView> extends MvpBasePresenter<V> {
    protected Context context;
    protected Preferences prefs;

    public BasePresenter(Context context) {
        if (prefs == null) {
            prefs = new Preferences(context);
        }
        this.context = context;
    }

    public Preferences getPrefs() {
        return prefs;
    }

    @Override public void attachView(V view) {
        super.attachView(view);
    }

}
