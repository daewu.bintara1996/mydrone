package com.mydrone.models.Drone;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DroneList {
    @SerializedName("drone_id")
    @Expose
    private Integer droneId;
    @SerializedName("drone_name")
    @Expose
    private String droneName;
    @SerializedName("drone_image")
    @Expose
    private String droneImage;
    @SerializedName("create_at")
    @Expose
    private String createAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("drone_author_id")
    @Expose
    private Integer droneAuthorId;
    @SerializedName("drone_reg_id")
    @Expose
    private String droneRegId;
    @SerializedName("drone_status")
    @Expose
    private String droneStatus;
    @SerializedName("drone_online_status")
    @Expose
    private String droneOnlineStatus;

    public Integer getDroneId() {
        return droneId;
    }

    public void setDroneId(Integer droneId) {
        this.droneId = droneId;
    }

    public String getDroneName() {
        return droneName;
    }

    public void setDroneName(String droneName) {
        this.droneName = droneName;
    }

    public String getDroneImage() {
        return droneImage;
    }

    public void setDroneImage(String droneImage) {
        this.droneImage = droneImage;
    }

    public String getCreateAt() {
        return createAt;
    }

    public void setCreateAt(String createAt) {
        this.createAt = createAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Integer getDroneAuthorId() {
        return droneAuthorId;
    }

    public void setDroneAuthorId(Integer droneAuthorId) {
        this.droneAuthorId = droneAuthorId;
    }

    public String getDroneRegId() {
        return droneRegId;
    }

    public void setDroneRegId(String droneRegId) {
        this.droneRegId = droneRegId;
    }

    public String getDroneStatus() {
        return droneStatus;
    }

    public void setDroneStatus(String droneStatus) {
        this.droneStatus = droneStatus;
    }

    public String getDroneOnlineStatus() {
        return droneOnlineStatus;
    }

    public void setDroneOnlineStatus(String droneOnlineStatus) {
        this.droneOnlineStatus = droneOnlineStatus;
    }
}
