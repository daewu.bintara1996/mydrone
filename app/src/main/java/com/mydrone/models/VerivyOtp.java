package com.mydrone.models;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VerivyOtp {

    @SerializedName("act")
    @Expose
    private String act;
    @SerializedName("author_id")
    @Expose
    private String authorId;
    @SerializedName("otp_code")
    @Expose
    private String otpCode;

    public String getAct() {
        return act;
    }

    public void setAct(String act) {
        this.act = act;
    }

    public String getAuthorId() {
        return authorId;
    }

    public void setAuthorId(String authorId) {
        this.authorId = authorId;
    }

    public String getOtpCode() {
        return otpCode;
    }

    public void setOtpCode(String otpCode) {
        this.otpCode = otpCode;
    }
}
