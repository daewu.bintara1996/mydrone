package com.mydrone.models;

import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class Speed {
    public String speed;

    public Speed(){

    }

    public Speed(String speed) {
        this.speed = speed;
    }

}
