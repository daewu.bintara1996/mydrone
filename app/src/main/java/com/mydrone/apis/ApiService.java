package com.mydrone.apis;

import com.mydrone.models.Drone.DroneUpdate;
import com.mydrone.models.Drone.RequestDrone;
import com.mydrone.models.Login;
import com.mydrone.models.VerivyOtp;

import java.util.Map;

import io.reactivex.Observable;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;


public interface ApiService {

    @FormUrlEncoded
    @POST("public/api/drone/author")
    Observable<Login> apiLoginRequest(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("public/api/drone/author")
    Observable<VerivyOtp> apiVerifyOtpRequest(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("public/api/drone/drone")
    Observable<RequestDrone> apiDroneRequest(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("public/api/drone/drone")
    Observable<DroneUpdate> apiDroneUpdateTest(@FieldMap Map<String, String> params);
//
//    @POST("APIbisnismu.php")
//    @FormUrlEncoded
//    Observable<RequestProduct> apiPPOBProductRequest(@FieldMap Map<String, String> params);
//
//    @POST("APIbisnismu.php")
//    @FormUrlEncoded
//    Observable<RequestCashback> apiPPOBCashbackRequest(@FieldMap Map<String, String> params);
//
//    @POST("APIbisnismu.php")
//    @FormUrlEncoded
//    Observable<RequestTransaction> apiPPOBTransactionRequest(@FieldMap Map<String, String> params);
//
//    @FormUrlEncoded
//    @POST("APIsetting.php")
//    Observable<ArrayList<Bank>> apiBankRequest(@FieldMap HashMap<String, String> params);
//
//    @FormUrlEncoded
//    @POST("APIuser.php")
//    Observable<RequestDeposit> apiDepositRequest(@FieldMap HashMap<String, String> params);
//
//    @FormUrlEncoded
//    @POST("APIinquiry.php")
//    Observable<RequestInquiry> apiInquiryPLNRequest(@FieldMap Map<String, String> params);
//
//    @FormUrlEncoded
//    @POST("APIpayment.php")
//    Observable<RequestPayment> apiPaymentRequest(@FieldMap Map<String, String> params);
//
//    @FormUrlEncoded
//    @POST("APIstruk.php")
//    Observable<RequestStruck> apiStruckRequest(@FieldMap Map<String, String> params);
//
//    @FormUrlEncoded
//    @POST("APIrequest.php")
//    Observable<RequestPayment> apiPurchaseRequest(@FieldMap Map<String, String> params);
//
//    @Multipart
//    @POST("APIuser.php  ")
//    Observable<RequestBase> apiUserRequestUpload(
//            @Part("act") RequestBody act,
//            @Part("customerId") RequestBody customerId,
//            @Part MultipartBody.Part image
//    );
//
//    @Multipart
//    @POST("APIpesan.php")
//    Observable<RequestPesan> apiSendMessageRequest(
//            @Part("act") RequestBody act,
//            @Part("idPesan") RequestBody idMsg,
//            @Part("isi") RequestBody txtmsg,
//            @Part MultipartBody.Part imgFile
//    );
//
//    @FormUrlEncoded
//    @POST("APIpesan.php")
//    Observable<RequestPesan> apiPesanRequest(@FieldMap Map<String, String> params);
//
//    @Multipart
//    @POST("APIpesan.php")
//    Observable<RequestPesan> apiCreateMessageRequest(
//            @Part("act") RequestBody act,
//            @Part("customerId") RequestBody customerId,
//            @Part("divisi") RequestBody divisi,
//            @Part("sub") RequestBody sub,
//            @Part("isi") RequestBody isi,
//            @Part MultipartBody.Part imgFile);
//
//
//    //PRODUCT ECOMMERCE
//    @FormUrlEncoded
//    @POST("APIproduk.php")
//    Observable<RequestProductStore> apiProductRequest(@FieldMap Map<String, String> params);
//
//    @FormUrlEncoded
//    @POST("APIproduk.php")
//    Observable<RequestCategory> apiProductCategoryRequest(@FieldMap Map<String, String> params);
//
//    //CART API
//    @FormUrlEncoded
//    @POST("APIcart.php")
//    Observable<RequestShipping> apiShippingRequest(@FieldMap Map<String, String> params);
//
//    @FormUrlEncoded
//    @POST("APIcart.php")
//    Observable<RequestCheckout> apiCheckoutRequest(
//            @FieldMap Map<String, String> params
//    );
//
//    @FormUrlEncoded
//    @POST("APIcart.php")
//    Observable<RequestOrder> apiOrderRequest(
//            @FieldMap Map<String, String> params
//    );
//
//    @FormUrlEncoded
//    @POST("APIpages.php")
//    Observable<RequestPage> apiPageRequest(
//            @FieldMap Map<String, String> params
//    );

//    @FormUrlEncoded
//    @POST("APIairline.php")
//    Observable<ApiStationRequest> apiStationRequest(
//            @FieldMap Map<String, String> options
//    );
//
//    @FormUrlEncoded
//    @POST("APIairline.php")
//    Observable<ApiSearchFlightRequest> apiSearchFlightRequest(
//            @FieldMap Map<String, String> options
//    );
//
//    @POST("APIairline.php")
//    Observable<BookingRespons> apiBookingRequest(
//            @Body BookingRequest bookingRequest
//    );
//
//    @FormUrlEncoded
//    @POST("APIairline.php")
//    Observable<BookingHistoryRespon> apiBookingHistoryRequest(@FieldMap Map<String, String> params);
//
//    @FormUrlEncoded
//    @POST("APIairline.php")
//    Observable<JsonObject> apiBookingPaymentRequest(@FieldMap Map<String, String> params);
//
//    @FormUrlEncoded
//    @POST("APIairline.php")
//    Observable<JsonObject> apiBookingCancelRequest(@FieldMap Map<String, String> params);
//
//    @FormUrlEncoded
//    @POST("APIairline.php")
//    Observable<JsonObject> apiBookingMailRequest(@FieldMap Map<String, String> params);
//
//    @FormUrlEncoded
//    @Streaming
//    @POST("APIairline.php")
//    Call<ResponseBody> apiDownloadRequest(@FieldMap HashMap<String, String> params);
}
